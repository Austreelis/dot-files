#!/usr/bin/env bash
# Morgane Austreelis' session launching file
# Run when a user logs in (I run when i3 starts, see i3 config file)

# Needs to be defined for some programs because of the way I arranged my home dir
export XDG_CONFIG_HOME="/home/morgane"
export XDG_CONFIG_MUSIC="/home/morgane/doc/music"
export XDG_CONFIG_PICTURES="/home/morgane/doc/picts"
export XDG_CONFIG_VIDEOS="/home/morgane/doc/videos"
export XDG_CONFIG_DESKTOP="/home/morgane/doc"
export XDG_CONFIG_PUBLIC="/home/morgane/doc/public"
export XDG_CONFIG_DOWNLOADS="/home/morgane/doc/dwl"

# Vim as default editor
export EDITOR=/usr/bin/vim

# Cowsay files paths
export COWPATH="/usr/share/cows:$XDG_CONFIG_HOME/doc/config/cowsay-files/cows"

# Some commad aliases for dmenu
 # pycharm launch command
# alias pycharm=.local/share/JetBrains/Toolbox/apps/PyCharm-P/ch-0/192.7142.42/bin/pycharm.sh

# Set backgrounds slideshow
# /!\ needs a running X11 session
# now using nitrogen
# watch -n 120 feh --randomize --bg-fill ~/doc/picts/backgrounds/* &

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar 222222222222>/dev/null; do sleep 0.1; done

# Launch polybars
for bar in music info info-left info-right; do
    echo "<<<---===--->>>" >> /tmp/polybar-$bar.log
    polybar --config=$HOME/doc/config/polybar/config $bar >> /tmp/polybar-$bar.log 2>&1 || echo "Couldn't launch bar $bar" &
done

echo ""

#Launch conky
~/doc/config/conky/startconky.sh

