""" Morgan's Leclerc vim config file

""******************************************************************************
"" config
""******************************************************************************

"" General

syntax enable     " Enable syntax highlighting
set number        " Show line numbers
set showbreak=¬¬  " Wrap-broken line prefix
set showmatch     " Highlight matching brace
 
set hlsearch   " Highlight all search results
set smartcase  " Enable smart-case search
set ignorecase " Always case-insensitive
set incsearch  " Searches for strings incrementally
 
set autoindent    " Auto-indent new lines
set expandtab     " Use spaces instead of tab characters
set shiftwidth=4  " Number of spaces per indent
set smartindent   " Enable smart-indent
set smarttab      " Make <tab> press go to next indent
set tabstop=4     " Number of spaces per hard tabs
set softtabstop=4 " Number of spaces per soft tabs
 
set confirm       " Prompt confirmation dialogs
set showtabline=2 " Show tab bar
 
set undolevels=10000           " Number of undo levels
set backspace=indent,eol,start " Backspace modern behaviour

"" Leader key
set showcmd     " Show when leader is press and *when it disappears*
let mapleader="," "Custom leader key, more convenient to me than \

"" C
autocmd FileType c setlocal cindent
autocmd FileType h setlocal cindent
autocmd FileType cpp setlocal cindent

"" Python
autocmd FileType py textwidth=80 colorcolumn=80 formatoptions+=croq cinwords=if,elif,else,while,for,try,except,finally,def,class,with

"" Latex
autocmd Filetype tex setlocal shiftwidth=2 softtabstop=2 tabstop=2 expandtab
let g:tex_flavor = "latex"

"" Buffers
set hidden


""******************************************************************************
"" Plugins
""******************************************************************************

runtime ftplugin/python.vim
runtime ftplugin/man.vim

call plug#begin()
" On-demand loading
Plug 'lervag/vimtex'
Plug 'justmao945/vim-clang' 
Plug 'rhysd/vim-clang-format'
Plug 'xavierd/clang_complete'
Plug 'tpope/vim-fugitive'
Plug 'chrisbra/Colorizer'
Plug 'Procrat/oz.vim'
call plug#end()


""******************************************************************************
"" Mappings
""******************************************************************************

" Text navigation
inoremap <C-e> <C-o>$
inoremap <C-a> <C-o>^

" Windows navigation
noremap <C-S-Left> <C-w>h
noremap <C-S-Down> <C-w>j
noremap <C-S-Up> <C-w>k
noremap <C-S-Right> <C-w>l

" Buffer navigation
noremap <C-Left> :bp<CR>
noremap <C-Right> :bn<CR>
noremap <C-D> :bd<CR>

" Git
noremap <Leader>ga :Gwrite<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gP :Gpush<CR>
noremap <Leader>gp :Gpull<CR>
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gd :Gvdiff<CR>
noremap <Leader>gr :Gremove<CR>

" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
