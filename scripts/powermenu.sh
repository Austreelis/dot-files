#!/bin/bash

## Created By Aditya Shakya

# MENU="$(echo -e "> Cancel\n> Lock\n> Logout\n> Reboot\n> Shutdown" | dmenu -i -l 5 -x 1710 -y 36 -w 200 -p '' -fn 'Awesome-15:bold' -nf '#0099CC' -nb '#333366' -sb '#0066FF' -sf '#CCFFFF')"
MENU="$(rofi -sep "|" -dmenu -i -theme 'DarkBlue'  -p 'System' -location 3 -xoffset -10 -yoffset 36 -width 10 -hide-scrollbar -line-padding 4 -padding 20 -lines 5 <<< "> Cancel|> Lock|> Logout|> Reboot|> Shutdown")"
            case "$MENU" in
                *Lock) xscreensaver-command -lock;;
                *Logout) i3-msg exit;;
                *Reboot) systemctl reboot ;;
                *Shutdown) systemctl -i poweroff
            esac
